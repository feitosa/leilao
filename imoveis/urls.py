# -*- coding: utf-8 -*-
from django.conf.urls import url

from imoveis.views import cadastrar_imovel, listar_imovel

from imoveis.views import apagar

urlpatterns = [
    url(r'^adicionar_imovel/$', cadastrar_imovel, name='adicionar_imovel'),
    url(r'^listar_imovel/$', listar_imovel, name='listar_imovel'),
    url(r'^apagar_imovel/(?P<pk>[^/]+)/$', apagar, name='apagar_imovel'),

]