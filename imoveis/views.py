from django.forms import modelform_factory
from django.shortcuts import render, redirect, render_to_response


# Create your viewsexe here.
from django.template import RequestContext
from django.urls import reverse_lazy, reverse

from imoveis.models import Imovel

ImovelForm = modelform_factory(Imovel, fields=('__all__'))

def cadastrar_imovel(request):
    if request.method == 'POST':
        imovel_form = ImovelForm(request.POST, request.FILES)
        if imovel_form.is_valid():
            imovel_form.save()
            return redirect(reverse_lazy('listar_imovel'))
    else:
        imovel_form = ImovelForm()

    return render(request, 'imovel_form.html', {'imovel_form': imovel_form})



def listar_imovel(request):
    imoveis = Imovel.objects.all()

    context = RequestContext(
        request, {
            'imoveis': imoveis,
        }
    )
    return render_to_response('listar_imovel.html', context)


def apagar(request, pk):
    if request.method == 'POST':
        imovel = Imovel.objects.get(pk=pk)
        imovel.delete()
    return redirect(reverse('listar_imovel'))