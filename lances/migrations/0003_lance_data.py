# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-11-06 12:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lances', '0002_lance_lote'),
    ]

    operations = [
        migrations.AddField(
            model_name='lance',
            name='data',
            field=models.DateTimeField(auto_now_add=True, default='2017-11-06 09:32:00'),
            preserve_default=False,
        ),
    ]
