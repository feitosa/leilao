# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-11-03 20:03
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lotes', '0002_remove_lote_lances'),
        ('lances', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='lance',
            name='lote',
            field=models.ForeignKey(default='01', on_delete=django.db.models.deletion.CASCADE, to='lotes.Lote'),
            preserve_default=False,
        ),
    ]
