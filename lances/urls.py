# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import lances

urlpatterns = [

    #url(r'^ofertar_lance/$', ofertar_lance, name='ofertar_lance'),
    url(r'^lances/(?P<pk>[^/]+)/$', lances, name='lances'),

]