# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import *


class LanceAdmin(admin.ModelAdmin):
    list_display =  '__all__'

admin.site.register(Lance)