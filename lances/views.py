from django.shortcuts import render, render_to_response, redirect
from django.contrib import messages

# Create your viewsexe here.
from django.template import RequestContext
from django.urls import reverse_lazy

from leiloes.models import Leilao

from lances.forms import LanceForm

from lances.models import Lance

from lotes.models import Lote

from lances.utils import pega_maior_lance, lances_por_usuario


'''
def ofertar_lance(request):
    if request.method == 'POST':
        lance_form = LanceForm(request.POST, request.FILES)
        if lance_form.is_valid():
            lance = lance_form.save(commit=False)
            lance.usuario = request.user
            lance.valor_inicial = 10
            lance_form.save()
            lance_form.save_m2m()
            return redirect(reverse_lazy('listar_lotes'))
    else:
       lance_form = LanceForm()
    return render(request, 'ofertar_lance.html', {'lance_form': lance_form})


def verificar_maior_lance(request,pk):
    leilao = Leilao.objects.get(pk=pk)
    valor = leilao.lances.valor
    lances = leilao.lances.filter(valor)
    max(lances)

    context = RequestContext(
        request, {
            'leilao': leilao,
            'lance':lances,
        }
    )
    return render_to_response('verificar_maior_lance.html', context)




def verificar_lance_atual():

    return render_to_response()

'''

def lances(request, pk):
    lote = Lote.objects.get(pk=pk)
    lances = Lance.objects.filter(lote=lote)
    print(request.FILES)
    if request.method == 'POST':
        lance_form = LanceForm(request.POST, request.FILES)
        if lance_form.is_valid():
            lance = lance_form.save(commit=False)
            if lance.lance_valido(lote):
                lance.usuario = request.user
                lance.lote = lote
                lance.valor_total = lance.valor+lote.taxa_adm+(lance.valor*(lote.comissao/100)) 
                lance_form.save()
                lance_form.save_m2m()
                messages.success(request, "Lance enviado com sucesso!")
                return redirect(reverse_lazy('listar_lotes', kwargs={'pk': lote.leilao.pk}))
            else:
                messages.error(request, "O valor do lance é inválido!")
                lance_form = LanceForm()
    else:
        lance_form = LanceForm()

    if pega_maior_lance(lances)==0: lance_minimo = lote.valor_lance_minimo
    else: lance_minimo = pega_maior_lance(lances) + lote.incremento
    extra = {
        "maior_lance": pega_maior_lance(lances),
        "lance_minimo": lance_minimo,
    }

    return render(
        request, "lances.html",
        {
            'lance_form': lance_form,
            'lote': lote,
            'lances': lances,
            'extra': extra,
            'lances_por_user': lances_por_usuario(),
        }
    )