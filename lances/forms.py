from django import forms

from lances.models import Lance


class LanceForm(forms.ModelForm):

    class Meta:
        model = Lance
        fields =('valor',)

