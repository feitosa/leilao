from lances.models import Lance
from django.contrib.auth import get_user_model


def pega_maior_lance(lances):
    lista_valores = []
    for lance in lances:
        lista_valores.append(lance.valor)
    try:
        return max(lista_valores)
    except Exception:
        return 0

def lances_por_usuario():
    usuarios = get_user_model().objects.all()
    lances = Lance.objects.all()

    lista = []
    for user in usuarios:
        dic = {}
        valor_total,qtd_total = 0,0
        for lance in lances:
            if lance.usuario==user:
                valor_total+=lance.valor
                qtd_total+=1

        dic["nome"] = user
        dic["valor_total"] = valor_total
        dic["qtd_total"] = qtd_total
        lista.append(dic)
    
    return lista