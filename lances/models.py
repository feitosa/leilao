from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models


from usuarios.models import Usuario
from lotes.models import Lote


class Lance(models.Model):
    usuario = models.ForeignKey(Usuario, related_name="usuario_lance")
    valor = models.FloatField()
    valor_total = models.FloatField()
    lote = models.ForeignKey(Lote)
    data = models.DateTimeField(auto_now_add=True)


    def lance_valido(self,lote):
        lances = Lance.objects.filter(lote=lote).order_by('-valor')
        
        if(len(lances)==0): ultimo_valor = 0
        else: ultimo_valor = lances[0].valor
        
        if(self.valor<lote.valor_lance_minimo or self.valor<ultimo_valor+lote.incremento):
            print(self.valor,"Aqui")
            return False
        return True

    def __str__(self):
        return str(self.valor)

    class Meta:
        verbose_name = u"lance"
        verbose_name_plural = u"lances"
