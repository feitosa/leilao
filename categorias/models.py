# -*- coding: utf-8 -*-
from django.db import models


class Categoria(models.Model):
    titulo = models.CharField(max_length=200)

    def __unicode__(self):
        return self.titulo

    class Meta:
        verbose_name = u"categoria"
        verbose_name_plural = u"categorias"