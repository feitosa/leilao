# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import *


class CategoriaAdmin(admin.ModelAdmin):
    list_display =  '__all__'

admin.site.register(Categoria)