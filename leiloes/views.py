from django.forms import modelform_factory
from django.shortcuts import render, render_to_response, redirect

# Create your viewsexe here.
from django.template import RequestContext

from categorias.models import Categoria
from django.urls import reverse_lazy
from django.views.generic import CreateView
from leiloes.models import Leilao

from leiloes.forms import LeilaoForm

from leiloes.forms import LeilaoLoteFormSet

from lotes.forms import LoteForm

from lotes.models import Lote


def leiloes(request):
    categorias = Categoria.objects.all()
    leiloes = Leilao.objects.all()

    context = RequestContext(
        request, {
            'categorias': categorias,
            'leiloes': leiloes,
        }
    )
    return render_to_response('leiloes.html', context)



def listar_leiloes(request):
    leiloes = Leilao.objects.all()

    context = RequestContext(
        request, {
            'leiloes': leiloes,
        }
    )
    return render_to_response('lista_leiloes_admin.html', context)



def add_leilao(request):
    if request.method == 'POST':
        leilao_form = LeilaoForm(request.POST, request.FILES)
        if leilao_form.is_valid():
            leilao_form.save()
            return redirect(reverse_lazy('listar_leiloes'))
    else:
        leilao_form = LeilaoForm()
    return render(request,'add_leilao_admin.html', {'leilao_form': leilao_form})




def adicionar_leilao(request):
    if request.method == 'POST':
        leilao_form = LeilaoForm(request.POST, request.FILES)
        lote_form = LoteForm(request.POST, request.FILES)
        if leilao_form.is_valid():
            lote_form.save()
            leilao_form.save()
            return redirect(reverse_lazy('listar_leiloes'))
    else:
        leilao_form = LeilaoForm()
        lote_form = LoteForm()
    return render(request, 'adicionar_leilao.html', {'leilao_form': leilao_form, 'lote_form':lote_form})



class OrderView(CreateView):
    template_name = 'adicionar_leilao.html'

    form_class = modelform_factory(Leilao,fields=('titulo','leiloeiro','categoria','tipo','foto','status','edital','data','data_fechamento'))
    def get_context_data(self, **kwargs):
        context = super(OrderView, self).get_context_data(**kwargs)
        if self.request.POST:
            context['forms'] = LeilaoForm(self.request.POST)
            context['formset'] = LeilaoLoteFormSet(self.request.POST)
        else:
            context['forms'] = LeilaoForm()
            context['formset'] = LeilaoLoteFormSet()
        return context
    def form_valid(self, form):
        context = self.get_context_data()
        forms = context['forms']
        formset = context['formset']
        if forms.is_valid() and formset.is_valid():
            self.object = form.save()
            forms.instance = self.object
            formset.instance = self.object
            forms.save()
            formset.save()
            return redirect('listar_lotes')
        else:
            return self.render_to_response(self.get_context_data(form=form))


def visualizar_leilao(request, pk):
    leilao = Leilao.objects.get(pk=pk)

    lotes = Lote.objects.filter(leilao=leilao)

    context = RequestContext(
        request, {
            'leiloes': leilao,
            'lotes': lotes,
            'total_lotes': lotes.count(),
        }
    )
    return render_to_response('visualizar_leilao.html', context)