"""leiloes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include


from .views import *

urlpatterns = [
    url(r'^leiloes/', leiloes, name='leiloes'),
    url(r'^adicionar_leilao/', adicionar_leilao, name='adicionar_leilao'),
    url(r'^listar_leiloes/', listar_leiloes, name='listar_leiloes'),
    url(r'^crud/$', OrderView.as_view(), name='crud'),
    url(r'^add_leilao/', add_leilao, name='add_leilao'),
    url(r'^visualizar_leilao/(?P<pk>[^/]+)/$', visualizar_leilao, name='visualizar_leilao'),
]