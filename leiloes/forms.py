from django import forms
from django.forms import inlineformset_factory
from leiloes.models import Leilao

from lotes.models import Lote

from lotes.forms import LoteForm



class LeilaoForm(forms.ModelForm):
    class Meta:
        model = Leilao
        fields = ('titulo','leiloeiro','categoria','tipo','foto','status','edital','data')
        widgets = {
            'data': forms.widgets.DateInput(attrs={'class': 'date-picker'}),
        }

    class Media:
        js = ('js/date-picker.js',)


LeilaoLoteFormSet = inlineformset_factory(Leilao, Lote, form=LoteForm, extra=4)
