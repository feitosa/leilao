# -*- coding: utf-8 -*-
from django.db import models

from categorias.models import Categoria

from leiloeiros.models import Leiloeiro


class Leilao(models.Model):
    TIPO_PRESENCIAL = 1
    TIPO_ONLINE = 2
    TIPO_PRESENCIAL_E_ONLINE = 3


    TIPO_CHOICES = (
        ( TIPO_PRESENCIAL, u'Presencial'),
        (TIPO_ONLINE, u'Online'),
        (TIPO_PRESENCIAL_E_ONLINE, u'Presencial e Online'),
    )

    ABERTO = 1
    FECHADO = 2
    EM_LOTEAMENTO = 3

    STATUS_CHOICES = (
        (ABERTO, u'Aberto Para Lance'),
        (FECHADO, u'Fechado Para Lance'),
        (EM_LOTEAMENTO, u'Em Loteamento'),
    )

    titulo = models.CharField(max_length=200)
    leiloeiro = models.ForeignKey(Leiloeiro, related_name="leiloeiros", blank=False)
    categoria = models.ForeignKey(Categoria, related_name="categorias", blank=False)
    data = models.DateTimeField()
    data_fechamento = models.DateTimeField()
    tipo = models.PositiveIntegerField(u"tipo", choices=TIPO_CHOICES, default=TIPO_PRESENCIAL)
    foto = models.ImageField(upload_to="foto_leilao/")
    status = models.PositiveIntegerField(u"status", choices=STATUS_CHOICES, default=FECHADO)
    edital = models.FileField(upload_to='editais')

    @property
    def str_status(self):
        return self.get_status_display()

    @property
    def str_tipo(self):
        return self.get_tipo_display()



    def __unicode__(self):
        return self.titulo

    class Meta:
        verbose_name = u"leilão"
        verbose_name_plural = u"leilões"
