from django.shortcuts import render, render_to_response, redirect

# Create your viewsexe here.
from django.template import RequestContext
from django.urls import reverse_lazy

from lotes.models import Lote

from leiloes.models import Leilao

from lances.models import Lance

from lotes.forms import LoteForm


def adicionar_lote(request):
    if request.method == 'POST':
        lote_form = LoteForm(request.POST, request.FILES)
        if lote_form.is_valid():
            lote_form.save()
            return redirect(reverse_lazy('listar_lotes'))
    else:
        lote_form = LoteForm()
    return render(request, 'adicionar_lote.html', {'lote_form': lote_form})



def listar_lotes(request,pk):
    leilao = Leilao.objects.get(pk=pk)
    lotes = Lote.objects.filter(leilao=leilao)
    lances = Lance.objects.all()


    context = RequestContext(
        request, {
            'leilao': leilao,
            'lotes': lotes,
            'lances': lances,
        }
    )
    return render_to_response('lista_lotes.html', context)


def detalhe_lote(request, pk):
    leilao = Leilao.objects.get(pk=pk)

    context = RequestContext(
        request, {
            'leilao': leilao,
        }
    )
    return  render_to_response('visualizar_leilao.html', context)