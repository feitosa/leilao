from django import forms

from lotes.models import Lote


class LoteForm(forms.ModelForm):
    class Meta:
        model = Lote
        fields = '__all__'
