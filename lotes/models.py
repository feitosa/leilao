# -*- coding: utf-8 -*-
from django.db import models

from fotos.models import Imagem
from usuarios.models import Comitente

from leiloes.models import Leilao


class Lote(models.Model):
    titulo = models.CharField(max_length=200)
    leilao = models.ForeignKey(Leilao, related_name="leilao", blank=True, null=True)
    num_lote = models.IntegerField()
    descricao = models.CharField(max_length=400)
    fotos = models.ForeignKey(Imagem, related_name="fotos_lotes", blank=False)
    comitente = models.ForeignKey(Comitente, related_name="comitente", blank=True, null=True)
    # Incremento mínimo: é a mínima quantia de dinheiro que será aceita para propor uma nova
    # oferta.Exemplo: se um item está R$ 10.000 com incremento mínimo de R$ 500, você só poderá
    # oferecer um lance se for maior ou igual a R$ 10.500.
    incremento = models.FloatField()
    taxa_adm = models.FloatField()
    comissao = models.FloatField()
    visitas_lote = models.IntegerField()
    valor_lance_minimo = models.FloatField()


    def __str__(self):
        return self.titulo


    @property
    def todos_lotes(self):
        return Lote.objects.filter(leilao=self.leilao)


    class Meta:
        verbose_name = u"lote"
        verbose_name_plural = u"lotes"
