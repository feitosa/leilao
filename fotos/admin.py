# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import *

class FotosAdmin(admin.ModelAdmin):
    list_display =  '__all__'

admin.site.register(Album)

class FotoAdmin(admin.ModelAdmin):
    list_display =  '__all__'

admin.site.register(Imagem)
