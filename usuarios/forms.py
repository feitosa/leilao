# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from django.apps import apps
from django.contrib.auth.models import Group, Permission
from django_select2.forms import ModelSelect2Widget
from localflavor.br.forms import BRCPFField, BRCNPJField
from .models import Usuario, Pessoa, Arrematante, Comitente


class UsuarioForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ('pessoa', 'login', 'cbo', 'n_conselho', 'tipo', 'ativo')
        widgets = {
            'admissao': forms.widgets.DateInput(
                attrs={'class': 'date-picker'}),
            'pessoa': ModelSelect2Widget(model=Pessoa,
                                         search_fields=['nome__icontains']),
        }

    class Media:
        js = ('js/date-picker.js',)


class PessoaForm(forms.ModelForm):
    cpf_cnpj = BRCPFField(label='CPF', required=False)

    class Meta:
        model = Arrematante
        fields = ('__all__')
        widgets = {
            'data_nascimento': forms.widgets.DateInput(
                attrs={'class': 'date-picker'}),
        }

    class Media:
        js = ('js/date-picker.js', 'js/jquery.mask.min.js', 'js/cpf.js')


class CustomModelMultiChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        config = apps.get_app_config(obj.content_type.app_label)
        model = config.get_model(obj.content_type.model)
        return '%s > %s > %s' % (
            config.verbose_name, model._meta.verbose_name, obj.name)


class GrupoForm(forms.ModelForm):
    name = forms.CharField(disabled=True, label='Nome')
    permissions = CustomModelMultiChoiceField(
        Permission.objects.exclude(content_type__app_label__in=[
            'auth', 'admin', 'contenttypes', 'sessions', 'table', 'auditlog']),
        widget=forms.widgets.CheckboxSelectMultiple, required=False,
        label='Permissões')

    class Meta:
        model = Group
        fields = ('__all__')