# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.contrib.auth.decorators import login_required, permission_required
from .models import Usuario, Arrematante
from .forms import UsuarioForm, PessoaForm, GrupoForm
from django.contrib.auth.models import Group


@login_required
@permission_required('pessoa.change_usuario', raise_exception=True)
def listar(request):
    usuarios = Usuario.objects.all()
    return render(request, 'pessoa/usuario/listar.html',
                  {'usuarios': usuarios})


@login_required
@permission_required('pessoa.change_usuario', raise_exception=True)
def adicionar(request):
    if request.method == 'POST':
        form = UsuarioForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('listar'))
    else:
        form = UsuarioForm()

    return render(request, 'usuario/form.html', {'form': form})


@login_required
@permission_required('pessoa.change_usuario', raise_exception=True)
def editar(request, id):
    usuario = Usuario.objects.get(pk=id)
    if request.method == 'POST':
        form = UsuarioForm(request.POST, instance=usuario)
        if form.is_valid():
            form.save()
            return redirect(reverse('listar'))
    else:
        form = UsuarioForm(instance=usuario)

    return render(request, 'pessoa/usuario/form.html',
                  {'object': usuario, 'form': form})


@login_required
@permission_required('pessoa.change_usuario', raise_exception=True)
def definir_senha(request, id):
    usuario = Usuario.objects.get(pk=id)
    if request.method == 'POST':
        form = AdminPasswordChangeForm(usuario, request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('listar'))
    else:
        form = AdminPasswordChangeForm(usuario)

    return render(request, 'pessoa/usuario/form.html',
                  {'object': usuario, 'form': form})


@login_required
@permission_required('pessoa.change_usuario', raise_exception=True)
def apagar(request, pk):
    if request.method == 'POST':
        exame = Usuario.objects.get(pk=pk)
        exame.delete()
    return redirect(reverse('listar'))




#Pessoa

@login_required
@permission_required('pessoa.change_paciente', raise_exception=True)
def adicionar(request):
    if request.method == 'POST':
        form = PessoaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('pessoa_listar'))
    else:
        form = PessoaForm()
    return render(request, 'pessoa/paciente/form.html', {'form': form})


@login_required
@permission_required('pessoa.change_paciente', raise_exception=True)
def editar(request, id):
    pessoa = Arrematante.objects.get(id=id)

    if request.method == 'POST':
        form = PessoaForm(request.POST, instance=pessoa)
        if form.is_valid():
            form.save()
            return redirect(reverse('pessoa_listar'))
    else:
        form = PessoaForm(instance=pessoa)
    return render(request, 'pessoa/arrematante/form.html',
                  {'form': form, 'object': pessoa})


@login_required
@permission_required('pessoa.delete_arrematante', raise_exception=True)
def apagar(request, id):
    if request.method == 'POST':
        pessoa = Arrematante.objects.get(id=id)
        pessoa.delete()
    return redirect(reverse('pessoa_listar'))


@login_required
@permission_required('pessoa.change_paciente', raise_exception=True)
def listar(request):
    pessoas = Arrematante.objects.all()
    table = PacienteTable()
    return render(request, 'pessoa/paciente/listar.html',
                  {'table': table, 'pessoas': pessoas})


#grupo

@login_required()
@permission_required('auth.change_group', raise_exception=True)
def listar_grupo(request):
    grupos = Group.objects.all()
    return render(request, 'grupo/listar.html', {'grupos': grupos})


@login_required()
@permission_required('auth.change_group', raise_exception=True)
def editar_grupo(request, id):
    grupo = Group.objects.get(pk=id)
    if request.method == 'POST':
        form = GrupoForm(request.POST, instance=grupo)
        if form.is_valid():
            form.save()
            return redirect(reverse('grupo_listar'))
    else:
        form = GrupoForm(instance=grupo)

    return render(request, 'grupo/form.html',
                  {'object': grupo, 'form': form})





