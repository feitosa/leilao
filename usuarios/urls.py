# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.views import login, logout

from usuarios.views import adicionar

from usuarios.views import listar_grupo, editar_grupo

urlpatterns = [
    url(r'^adicionar_usuario/$', adicionar, name='adicionar_usuario'),


    url(r'^grupos/(?P<id>\d+)/editar$', editar_grupo,
        name='grupo_editar'),
    url(r'^grupos$', listar_grupo,
        name='grupo_listar'),
]