# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager,
                                        PermissionsMixin)
from django.utils.encoding import python_2_unicode_compatible
from djchoices import DjangoChoices, ChoiceItem
from django.utils.timezone import now
from auditlog.registry import auditlog


class UsuarioManager(BaseUserManager):
    def create_user(self, login, password=None):
        """
        Cria e salva usuário a partir de login e senha
        """
        if not login:
            raise ValueError('Usuário deve possuir login')

        user = self.model(login=login)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, login, password=None):
        """
        Cria e salva super usuário a partir de login e senha
        """
        user = self.create_user(login, password=password)
        user.tipo = Usuario.Tipos.administrador
        user.save(using=self._db)
        return user


class ArrematanteManager(models.Manager):
    def get_queryset(self):
        return super(ArrematanteManager, self).get_queryset().filter(tipo='p')


class ComitenteManager(models.Manager):
    def get_queryset(self):
        return super(ComitenteManager, self).get_queryset().filter(tipo='f')



@python_2_unicode_compatible
class Pessoa(models.Model):
    class Sexos(DjangoChoices):
        masculino = ChoiceItem(1, 'Masculino')
        feminino = ChoiceItem(2, 'Feminino')

    nome = models.CharField(max_length=50)
    tipo = models.CharField(max_length=1, default='p', editable=False)
    telefone = models.CharField(max_length=12, null=True)
    email = models.EmailField('e-mail', max_length=50, blank=True, null=True)
    # Pessoa fisica
    cpf_cnpj = models.CharField('CPF/CPNJ', max_length=18, blank=True,
                                null=True)
    data_nascimento = models.DateField('Nascimento', blank=True, null=True)
    sexo = models.IntegerField('sexo', choices=Sexos.choices,
                               blank=True, null=True)
    # Endereço
    logradouro = models.CharField(max_length=50, null=True)
    numero = models.CharField('Número', max_length=10, null=True)
    complemento = models.CharField(max_length=50, blank=True, null=True)
    bairro = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Pessoa'
        default_permissions = []


class Arrematante(Pessoa):
    objects = ArrematanteManager()

    def save(self, *kargs, **kwargs):
        self.tipo = 'p'
        return super(Arrematante, self).save(*kargs, **kwargs)

    class Meta:
        proxy = True
        default_permissions = []
        permissions = (
            ('change_arrematante', "Cadastrar arrematante"),
            ('delete_arrematante', "Excluir arrematante"),
        )


class Comitente(Pessoa):
    objects = ComitenteManager()

    def save(self, *kargs, **kwargs):
        self.tipo = 'f'
        return super(Comitente, self).save(*kargs, **kwargs)

    class Meta:
        proxy = True
        default_permissions = []
        permissions = (
            ('change_comitente', "Cadastrar comitente"),
            ('delete_comitente', "Excluir comitente"),
        )


@python_2_unicode_compatible
class Usuario(AbstractBaseUser, PermissionsMixin):
    class Tipos(DjangoChoices):
        administrador = ChoiceItem(1, 'Administrador')
        gerente = ChoiceItem(2, 'Leiloeiro')
        atentente = ChoiceItem(3, 'Arrematante')

    pessoa = models.OneToOneField(Pessoa, null=True, on_delete=models.CASCADE)
    login = models.CharField(max_length=20, unique=True)
    cbo = models.IntegerField('CBO', blank=True, null=True)
    n_conselho = models.IntegerField('Conselho de Classe',
                                     blank=True, null=True)
    admissao = models.DateField(default=now)
    ativo = models.BooleanField(default=True)
    tipo = models.IntegerField(choices=Tipos.choices, default=Tipos.atentente)

    objects = UsuarioManager()
    USERNAME_FIELD = 'login'

    def __str__(self):
        return self.get_short_name()

    @property
    def is_active(self):
        return self.ativo

    class Meta:
        verbose_name = 'Usuário'
        default_permissions = []
        permissions = (
            ('change_usuario', "Cadastrar"),
            ('log_acoes', "Relatorio de ações"),
        )

    # Para funcionamento correto do admin
    # <admin>
    @property
    def is_staff(self):
        return self.is_superuser

    @property
    def is_superuser(self):
        return self.tipo == Usuario.Tipos.administrador

    def get_short_name(self):
        if self.pessoa:
            return self.pessoa.nome.split(' ')[0]
        else:
            return self.login

    def get_full_name(self):
        if self.pessoa:
            return self.pessoa.nome
        else:
            return self.login
    # </admin>


