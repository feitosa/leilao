# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import *


class UsuarioAdmin(admin.ModelAdmin):
    list_display =  '__all__'

admin.site.register(Usuario)


class PessoaAdmin(admin.ModelAdmin):
    list_display =  '__all__'


class ArrematanteAdmin(admin.ModelAdmin):
    list_display =  '__all__'


admin.site.register(Pessoa)

admin.site.register(Arrematante)