"""leiloes_online URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin


from django.conf import settings

from .views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^leiloes/', include('leiloes.urls')),
    url(r'^usuarios/', include('usuarios.urls')),
    url(r'^lotes/', include('lotes.urls')),
    url(r'^lances/', include('lances.urls')),
    url(r'^pessoas/', include('pessoas.urls')),
    url(r'^$', diario_site, name='index'),
    url(r'^imoveis/',include('imoveis.urls')),
    url(r'^contato/', contato, name='contato'),
    url(r'^imovel/', imovel, name='imovel'),
    url(r'^administrador/', admin_site, name='administrador'),
    url(r'^blog/', include('zinnia.urls')),
    url(r'^blog/about', include('zinnia.urls')),
    url(r'^comments/', include('django_comments.urls')),
    url(r'^select2/', include('django_select2.urls')),
    # url(r'^youtube/', include('django_youtube.urls')),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
