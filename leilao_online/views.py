from django.contrib.auth.decorators import login_required
from django.shortcuts import render, render_to_response
from django.template import RequestContext

from imoveis.models import Imovel


def diario_site(request):
    return render(request, "index.html")


def contato(request):
    return render(request, "contact.html")

def imovel(request):
    imoveis = Imovel.objects.all()

    context = RequestContext(
        request, {
            'imoveis': imoveis,
        }
    )
    return render_to_response("portfolio.html", context)


def admin_site(request):
    return render(request, "inicio_admin.html")