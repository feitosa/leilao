from __future__ import unicode_literals

from django.db import models

from usuarios.models import Usuario


class Leiloeiro(models.Model):
    user = models.OneToOneField(Usuario, related_name="leiloeiro")
    nome = models.CharField(max_length=255)
    cpf = models.CharField(max_length=11)
    email = models.EmailField()
    telefone = models.CharField(max_length=20)
