# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import *


class LeiloeiroAdmin(admin.ModelAdmin):
    list_display =  '__all__'

admin.site.register(Leiloeiro)