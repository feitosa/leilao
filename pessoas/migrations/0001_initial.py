# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-11-03 18:03
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Pessoa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=200)),
                ('ativo', models.BooleanField(default=True)),
                ('telefone', models.IntegerField()),
                ('email', models.EmailField(max_length=254)),
            ],
            options={
                'verbose_name': 'pessoa',
                'verbose_name_plural': 'pessoas',
            },
        ),
        migrations.CreateModel(
            name='PF',
            fields=[
                ('pessoa_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='pessoas.Pessoa')),
                ('cpf', models.CharField(max_length=11, unique=True, verbose_name='CPF')),
                ('data_nascimento', models.DateField()),
                ('rg', models.CharField(max_length=11)),
                ('sexo', models.PositiveIntegerField(choices=[(1, 'Masculino'), (2, 'Feminino')], default=1, verbose_name='tipo')),
                ('status', models.PositiveIntegerField(choices=[(1, 'Solteiro'), (2, 'Casado'), (3, 'Divorciado'), (4, 'Viúvo')], default=1, verbose_name='estado civil')),
                ('usuario', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='pf', to=settings.AUTH_USER_MODEL)),
            ],
            bases=('pessoas.pessoa',),
        ),
        migrations.CreateModel(
            name='PJ',
            fields=[
                ('pessoa_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='pessoas.Pessoa')),
                ('nome_fantasia', models.CharField(max_length=100)),
                ('razao', models.CharField(max_length=200)),
                ('cnpj', models.CharField(max_length=14)),
                ('usuario', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='pj', to=settings.AUTH_USER_MODEL)),
            ],
            bases=('pessoas.pessoa',),
        ),
    ]
