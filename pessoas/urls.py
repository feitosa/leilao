# -*- coding: utf-8 -*-
from django.conf.urls import url

from pessoas.views import cadastrar_pf, cadastrar_pj, listar_pj

from pessoas.views import ItemListView, PFUpdateView

urlpatterns = [
    url(r'^adicionar_pf/$', cadastrar_pf, name='adicionar_pf'),
    url(r'^listar_pf/$', ItemListView.as_view(), name='listar_pf'),
    url(r'^editar_pf/(?P<pk>[^/]+)/$', PFUpdateView.as_view(), name='editar_pf'),
    url(r'^adicionar_pj/$', cadastrar_pj, name='adicionar_pj'),
    url(r'^listar_pj/$', listar_pj, name='listar_pj'),
]