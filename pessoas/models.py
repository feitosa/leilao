# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models

# Create your models here.


from usuarios.models import Usuario


class Pessoa(models.Model):
    nome = models.CharField(max_length=200)
    ativo = models.BooleanField(default=True)
    telefone = models.IntegerField()
    email = models.EmailField()


    class Meta:
        verbose_name = 'pessoa'
        verbose_name_plural = 'pessoas'


class PF(Pessoa):
    SEXO_MASCULINO = 1
    SEXO_FEMININO = 2

    SEXO_CHOICES = (
        (SEXO_MASCULINO, u'Masculino'),
        (SEXO_FEMININO, u'Feminino'),
    )

    STATUS_SOLTEIRO = 1
    STATUS_CASADO = 2
    STATUS_DIVORCIADO = 3
    STATUS_VIUVO = 4

    STATUS_CHOICES = (
        (STATUS_SOLTEIRO, u'Solteiro'),
        (STATUS_CASADO, u'Casado'),
        (STATUS_DIVORCIADO, u'Divorciado'),
        (STATUS_VIUVO, u'Viúvo')
    )

    usuario = models.OneToOneField(Usuario, related_name='pf')
    cpf = models.CharField(u'CPF', unique=True, max_length=11)
    data_nascimento = models.DateField()
    rg = models.CharField(max_length=11)
    sexo = models.PositiveIntegerField(u"tipo", choices=SEXO_CHOICES, default=SEXO_MASCULINO)
    status = models.PositiveIntegerField(u"estado civil", choices=STATUS_CHOICES, default=STATUS_SOLTEIRO)




class PJ(Pessoa):
    usuario = models.OneToOneField(Usuario, related_name='pj')
    nome_fantasia = models.CharField(max_length=100)
    razao = models.CharField(max_length=200)
    cnpj = models.CharField(max_length=14)
