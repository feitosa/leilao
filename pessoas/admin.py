# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import *


class PessoaAdmin(admin.ModelAdmin):
    list_display =  '__all__'

admin.site.register(Pessoa)


class PFAdmin(admin.ModelAdmin):
    list_display =  '__all__'

admin.site.register(PF)


class PJAdmin(admin.ModelAdmin):
    list_display =  '__all__'

admin.site.register(PJ)