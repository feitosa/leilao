from django.http import HttpResponse
from django.shortcuts import render, redirect, render_to_response

# Create your viewsexe here.
from django.template import RequestContext
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.views.generic import ListView, UpdateView, CreateView
from pessoas.forms import PFForm, PJForm

from pessoas.models import PF, PJ


def cadastrar_pf(request):
    if request.method == 'POST':
        form = PFForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('listar_pf')
    else:
        form = PFForm()
    context = RequestContext(request, {'form': form})
    return render_to_response('pf_form.html', context)

"""
pessoas list
"""
class ItemListView(ListView):
    model = PF
    template_name = 'listar_pf.html'

    def get_queryset(self):
        return PF.objects.all()


"""
Edit item
"""
class PFUpdateView(UpdateView):
    model = PF
    form_class = PFForm
    template_name = 'item_edit_form.html'

    def dispatch(self, *args, **kwargs):
        self.pf_id = kwargs['pk']
        return super(PFUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.save()
        pf = PF.objects.get(id=self.pf_id)
        return HttpResponse(render_to_string('item_edit_form_success.html', {'pf': pf}))



def cadastrar_pj(request):
    if request.method == 'POST':
        pj_form = PJForm(request.POST, request.FILES)
        if pj_form.is_valid():
            pj_form.save()
            return redirect('listar_pj')
    else:
        pj_form = PJForm()
    context = RequestContext(request, {'pj_form': pj_form})
    return render_to_response('pj_form.html', context)


def listar_pj(request):
    pjs = PJ.objects.all()

    context = RequestContext(
        request, {
            'pjs': pjs,
        }
    )
    return render_to_response('listar_pj.html', context)




class PJCreate(CreateView):
    form_class = PJForm
    template_name = 'pj_form.html'
    success_url = reverse_lazy('listar_pj')
