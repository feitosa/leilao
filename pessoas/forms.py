from django.contrib.auth.models import User

from usuarios.forms import UsuarioForm
from .models import PF, PJ
from django import forms


class PFForm(forms.ModelForm):

    class Meta:
        model = PF
        fields = [
            'nome',
            'ativo',
            'telefone',
            'email',
            'usuario',
            'cpf',
            'data_nascimento',
            'rg',
            'sexo',
            'status',
        ]


class PJForm(forms.ModelForm):
    class Meta:
        model = PJ
        fields = [
            'nome',
            'ativo',
            'telefone',
            'email',
            'usuario',
            'nome_fantasia',
            'razao',
            'cnpj',

        ]

    def __init__(self, *args, **kwargs):
        super(PJForm, self).__init__(*args, **kwargs)
        self.usuario_form = UsuarioForm(
            prefix='usuario'
        )

    def is_valid(self):
        if not self.usuario_form.is_valid():
            return False

        return super(PJForm, self).is_valid()

    def save(self, *args, **kwargs):
        usuario = self.usuario_form.save()

        pj = super(PJForm, self).save(*args, **kwargs)

        pj.usuario = usuario
        pj.save()

        return pj
